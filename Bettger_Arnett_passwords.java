/*
 * This program will generate random integers, translate the decimal value to its ascii code, and use the code to generate a password. 
 */
package datastructures;
import java.util.Random;
public class DataStructures {
    public static void main(String[] args){
        int passLength = 12;                //password length
        int numPass = 1000;                   //number of passwords to be generated
        int randNum;                        //to store the value returned by the random number generator
        char randChar;                      //to store the converted ascii value of the random number generated
        String[] passwords = new String[numPass];   //array of type string to store the passwords. 
        
        System.out.println("Generating random passwords");
        int Start = 33;                             //based on ascii table. Values should be between 33 and 90.
        int End = 90;
        Random random = new Random();               //creates an object of class Random. 
        for(int ndx=0; ndx<numPass; ndx++){         //will run through the number of passwords to be created. 
            passwords[ndx]="";          //Initializes password array at the current index location. (So it does not return a null)
            for(int idx=0; idx<passLength;idx++){       //runs through until passwords are the required length (12 characters)
                randNum=showRandomInt(Start, End, random);  //gets a random number between the given range, stores the value in randNum
                randChar=(char)randNum;                     //converts the randNum number to its ascii value.
                //System.out.println("Char: "+randChar);    //for testing purposes, not necessary to print. 
                passwords[ndx]+=randChar;                   //adds the ascii value to the password value at array location ndx
            }
            System.out.println("#"+(ndx+1)+": "+passwords[ndx]);               //prints out the pw number, then the password
        }
        System.out.println("Done.");                //prints once all passwords have been displayed. 
    }
    
    public static int showRandomInt(int numStart, int numEnd, Random aRandom){
        if (numStart > numEnd){
            throw new IllegalArgumentException("Start cannot exceed End.");     //This should never be triggered unless the start and end values are changed.
        }
        long range = (long)numEnd - (long)numStart +1;                   //The following code uses the Random class to produce a random number within the given range. 
        long fraction = (long) (range*aRandom.nextDouble());
        int randomNumber = (int) (fraction+numStart);
        //System.out.println("Generated : "+randomNumber);               //for testing purposes
        return randomNumber;
    }
}